﻿using UnityEngine;
using System.Collections;

// Script for creating objects (asteroids & rockets)

public class CreateObject : MonoBehaviour {

	// Prefabs
	public Transform asteroid;
	public Transform rocket;
	public Transform greenAsteroid;
	public Transform normalAsteroid;
	private Transform selectedObject;

	// Number of path (0 = left, 1 = middle and 2(or else) = right)
	private int pathNumber;
	private int prevPathNumber;

	private int objectNumber;

	// Possible start positions
	private Vector3 left;
	private Vector3 middle;
	private Vector3 right;
	private Vector3 startPosition;

	// Counter for how many asteroid prefabs have been created
	private int asteroidsCreated;

	// Timer for creating objects
	private float timer;

	private int createSwitch;

	// Timer for stopping obejct generation
	//private float instantiateTimer;

	// Switch whether instantiate is on (true) or off (false)
	//private bool createOn;
	public static bool createOn;

	// Idicates how often objects are created
	public static float createTime;

	// xy-coordinates for possible start positions
	private float leftX = -10f;
	private float leftY = 6f;
	private float middleX = 0f;
	//private float middleY = 9.3137f;
	private float middleY = 9.4137f;
	private  float rigthX = 10f;
	private float rigthY = 6f;

	// Object's rotation
	private Vector3 rotation;

	public Transform asteroidExplosion;

	void Start () {

		asteroid = normalAsteroid;
		left = new Vector2(leftX,leftY);
		middle = new Vector2(middleX,middleY);
		right = new Vector2(rigthX,rigthY);
		createTime = 2f;
		asteroidsCreated = 0;
		createSwitch = 0;
		createOn = false;
		prevPathNumber = 0;
		timer = 0f;
		//instantiateTimer = 0;

	}

	void Update () {

		timer += Time.deltaTime;
		//instantiateTimer += Time.deltaTime; 

		CheckIfCreateObject ();

		if (timer >= createTime && createOn == true) {
			DrawStartPosition();
			DrawObjectToInstantiate ();
			Instantiate (selectedObject, startPosition, Quaternion.Euler (rotation));
			timer = 0f;
		}
	}
		
	// Draws random start position for an object and sets the rotation
	void DrawStartPosition() {

		pathNumber = Random.Range (0, 3);
		if (pathNumber != prevPathNumber) {

			if (pathNumber == 0) {
				startPosition = left;
				rotation = new Vector3 (0, 0, 45);
			} else if (pathNumber == 1) {
				startPosition = middle;
				rotation = new Vector3 (0, 0, 0);
			} else {
				startPosition = right;
				rotation = new Vector3 (0, 0, 315);
			}

		} else {
			DrawStartPosition ();
		}
		prevPathNumber = pathNumber;
	}		

	// Draws which objects is going to be created next
	void DrawObjectToInstantiate() {

			objectNumber = Random.Range (0, 4);
			if (objectNumber == 0) {
				selectedObject = rocket;
				asteroidsCreated++;
			} else {
				selectedObject = asteroid;
			}

	}

	// Check whether prefabs should be created or not
	void CheckIfCreateObject(){

		GameObject[] asteroids = GameObject.FindGameObjectsWithTag("Asteroid");


		if (Score.score == 10 && createSwitch == 0) {
			//instantiateTimer = 0f;
			foreach (GameObject ast in asteroids) {
				Instantiate(asteroidExplosion, new Vector2(ast.transform.position.x, ast.transform.position.y), Quaternion.identity);
				GameObject.Destroy (ast);
			}

			createSwitch = 1;
			createOn = false;
			asteroid = greenAsteroid;
		}
			/*if (instantiateTimer >= 11f) {
				createOn = true;
			}*/
	}
}
