﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndIntro : MonoBehaviour {

	private float timer;

	// Use this for initialization
	void Start () {

		timer = 0f;
	
	}
	void Update () {
	
		timer += Time.deltaTime;
		if (timer >= 5f) {
			StartCoroutine (ExitIntro ());
		}
	}

	IEnumerator ExitIntro(){

		float fadingTime = GameObject.Find("Main Camera").GetComponent<SceneFader>().StartFading(1);
		yield return new WaitForSeconds (2.5f);
		SceneManager.LoadScene ("Game");

	}
}
