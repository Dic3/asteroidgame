﻿using UnityEngine;
using System.Collections;

// Script for handling ufo's movement

public class UfoMovement : MonoBehaviour {

	// Ufo's target destination
	public Transform targetDestination2;

	// Ufo's speed
	float speed;

	// Use this for initialization
	void Start () {

		speed = 0.8f;
	
	}

	void FixedUpdate(){

		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, targetDestination2.position, step);
	
	}

	// Update is called once per frame
	void Update () {
	
	}
}