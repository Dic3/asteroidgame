﻿using UnityEngine;
using System.Collections;

public class UfoLight : MonoBehaviour {



	private Color lightColor;
	private SpriteRenderer ufoSpriteRenderer;

	void Start () {
	
		ufoSpriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
		StartCoroutine (FadeLightIn ());
	}

	void Update () {
	
	}

	public IEnumerator FadeLightIn(){

		lightColor = new Color32 (255, 255, 255, 25);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 50);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 75);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 100);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 125);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 150);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 175);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 200);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 225);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (.1f);
		lightColor = new Color32 (255, 255, 255, 255);
		ufoSpriteRenderer.color = lightColor;
		yield return new WaitForSeconds (4f);
		CreateObject.createOn = true;
	}
}
