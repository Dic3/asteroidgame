﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoBlink : MonoBehaviour {

	private Text text;
	private Color textColor;




	// Use this for initialization
	void Start () {

		text = gameObject.GetComponent<Text> ();
		StartCoroutine (ChangeColors ());

	}

	// Update is called once per frame
	void Update () {

			transform.Rotate (Vector3.right * 3.5f);

		}



	public IEnumerator ChangeColors(){

		for(int i = 0; i<2 ; i++) {
			textColor = new Color32 (147, 132, 239, 255);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 200);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 150);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 100);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 70);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 70);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 100);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 150);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 200);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (147, 132, 239, 255);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
		}
		textColor = new Color32 (147, 132, 239, 255);
		text.color = textColor;
		yield return new WaitForSeconds (.15f);
		textColor = new Color32 (147, 132, 239, 200);
		text.color = textColor;
		yield return new WaitForSeconds (.15f);
		textColor = new Color32 (147, 132, 239, 150);
		text.color = textColor;
		yield return new WaitForSeconds (.15f);
		textColor = new Color32 (147, 132, 239, 100);
		text.color = textColor;
		yield return new WaitForSeconds (.15f);
		textColor = new Color32 (147, 132, 239, 70);
		gameObject.SetActive (false);
		ButtonsAndPanels.pauseButton.GetComponent<Button> ().interactable = true;
		CreateObject.createOn = true;
	}
		
}