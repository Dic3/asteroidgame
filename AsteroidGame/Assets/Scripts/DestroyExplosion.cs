﻿using UnityEngine;
using System.Collections;

// Destroy explosion prefab once animation is played

public class DestroyExplosion : MonoBehaviour {

	private Animator anim;

	void Start () {

		anim = GetComponent<Animator> ();
		StartCoroutine(DestroyAnimation());
	}
	
	void Update () {}

	// Corutine for destroying animation
	private IEnumerator DestroyAnimation(){

		yield return new WaitForSeconds (anim.GetCurrentAnimatorStateInfo(0).length);
		Destroy (gameObject);
	}
}
