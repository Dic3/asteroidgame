﻿using UnityEngine;
using System.Collections;

// Controls the speed and moving of an object

public class ObjectMovement : MonoBehaviour {

	// Object's destination in screen
	public Transform targetDestination;

	// Object's speed
	public static float objectSpeed = 1f;

	void Start () {
		
	}

	void FixedUpdate(){

		float step = objectSpeed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, targetDestination.position, step);
	}

	void Update () {}

	// Increases the speed
	public static void IncreaseSpeed(){
		objectSpeed = objectSpeed + 0.2f;
	}
}


