﻿using UnityEngine;
using System.Collections;

public class SceneFaderGame : MonoBehaviour {

	public Texture2D fadingTexture;
	public float fadingSpeed =0.2f;
	private int drawDepth = -1000;
	private float aplha = 0.0f;
	private int fadingDir = -1;

	void OnGUI(){

		aplha += fadingDir * fadingSpeed * Time.deltaTime;
		aplha = Mathf.Clamp01 (aplha);
		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, aplha);
		GUI.depth = drawDepth;
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), fadingTexture);
	}

	public float StartFading (int direction){
			fadingDir = direction;
			return 1;
	}
}