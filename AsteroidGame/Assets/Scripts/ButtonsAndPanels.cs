﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Buttons and panels used in game

public class ButtonsAndPanels : MonoBehaviour {

	public static GameObject gameOverPanel;
	public GameObject pausePanel;
	public static GameObject pauseButton;
	public static GameObject pauseButtonPanel;
	public static GameObject confirmationPanelRestart;
	public static GameObject confirmationPanelExit;
	public static GameObject preFirePanel;
	public static GameObject firePanel;
	public static GameObject fireButton;
	public static GameObject helpPanel;
	private GameObject ammo;
	private GameObject errorScreen;
	public static bool preFirePanelEnabled;
	public static bool fireEnabled;

	public GameObject letsGoText;



	void Start () {
	
		fireEnabled = false;
		gameOverPanel = GameObject.Find ("GameOverPanel");
		pausePanel = GameObject.Find ("PausePanel");
		pauseButton = GameObject.Find ("Pause");
		pauseButtonPanel = GameObject.Find ("PauseButtonPanel");
		confirmationPanelRestart = GameObject.Find ("RestartConfirmationPanel");
		confirmationPanelExit = GameObject.Find ("ExitConfirmationPanel");
		firePanel = GameObject.Find ("FirePanel");
		fireButton = GameObject.Find ("Fire");
		preFirePanel = GameObject.Find ("PreFirePanel");
		ammo = GameObject.Find ("Ammo");
		errorScreen = GameObject.Find ("ErrorScreen");
		helpPanel = GameObject.Find ("HelpPanel");
		letsGoText = GameObject.Find ("GoText");
		letsGoText.SetActive (false);
		pauseButton.GetComponent<Button> ().interactable = false;
		fireButton.GetComponent<Button> ().interactable = true;
		preFirePanel.SetActive (false);
		firePanel.SetActive (false);
		ammo.SetActive (false);
		errorScreen.SetActive (false);
		gameOverPanel.SetActive (false);
		pausePanel.SetActive (false);
		confirmationPanelRestart.SetActive (false);
		confirmationPanelExit.SetActive (false);

		DisplayHelpPanelOrNot ();
	
	}

	void Update () {

		CheckIfEnablePreFirePanel ();
		CheckIfEnableFireButton ();
	}

	// Restart button
	public void Restart(){

		confirmationPanelRestart.SetActive (true);
		pausePanel.SetActive (false);
	}

	// If restart confirmed
	public void RestartConfirmed(){
	
		ResetValues.ResetAllValues ();
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

	}

	// If restart denined
	public void RestartDenided(){
		confirmationPanelRestart.SetActive (false);
		pausePanel.SetActive (true);
	}

	// Exit button
	public void Exit(){

		confirmationPanelExit.SetActive (true);
		pausePanel.SetActive (false);
	}

	// If exit confirmed
	public void ExitConfirmed(){
	
		ResetValues.ResetAllValues ();
		SceneManager.LoadScene ("Main");
	}

	// If exit denided
	public void ExitDenided(){
	
		confirmationPanelExit.SetActive (false);
		pausePanel.SetActive (true);
	}
		
	// Pause button
	public void Pause(){

		Time.timeScale = 0;
		pausePanel.SetActive (true);
		pauseButton.GetComponent<Button> ().interactable = false;
		fireButton.GetComponent<Button> ().interactable = false;
		PlayerMovement.movable = false;
	}

	// Resume button
	public void Resume(){

		Time.timeScale = 1;
		pausePanel.SetActive (false);
		pauseButton.GetComponent<Button> ().interactable = true;
		fireButton.GetComponent<Button> ().interactable = true;
		PlayerMovement.movable = true;
	}

	// Play Again button
	public void PlayAgain(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	// Fire button
	public void Fire(){
		ammo.SetActive (true);
	}

	// Play button in Instructions panel
	public void InsPanelPlay(){
		helpPanel.SetActive (false);
		letsGoText.SetActive (true);
		letsGoText.GetComponent<GoBlink> ().enabled = true;
	}

	void CheckIfEnableFireButton(){
		if (fireEnabled == true) {
			firePanel.SetActive (true);
			preFirePanel.SetActive (false);
		}
	}

	void CheckIfEnablePreFirePanel(){
		if (preFirePanelEnabled == true) {
			preFirePanel.SetActive (true);
		}
	}
		
	void DisplayHelpPanelOrNot ()
	{
		if (!PlayerPrefs.HasKey("disphelp")) {
			helpPanel.SetActive (true);
			PlayerPrefs.SetInt("disphelp",1);
		} else {
			helpPanel.SetActive(false);
			letsGoText.SetActive (true);
			letsGoText.GetComponent<GoBlink> ().enabled = true;
		}
	}
}
