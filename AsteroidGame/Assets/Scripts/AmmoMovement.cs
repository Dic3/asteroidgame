﻿using UnityEngine;
using System.Collections;

// Script for handling ammo's movement

public class AmmoMovement : MonoBehaviour {

	// Ammo's target destination
	public Transform targetDestination3;

	// Ammo's speed
	float speed;

	void Start () {

		speed = 1.5f;
	}

	void FixedUpdate(){

		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, targetDestination3.position, step);

	}
		
	void Update () {

	}
}
