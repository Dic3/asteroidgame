﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainButtonsAndPanels : MonoBehaviour {

	public static GameObject settingsPanel;
	public static GameObject mainButtonsPanel;
	public static GameObject helpPanel;
	public static GameObject settingsButtonPanel;
	public static GameObject exitButtonPanel;


	// Use this for initialization
	void Start () {
	
		settingsPanel = GameObject.Find ("SettingsPanel");
		settingsPanel.SetActive (false);
		mainButtonsPanel = GameObject.Find("MainButtons");
		mainButtonsPanel.SetActive (true);
		helpPanel = GameObject.Find("HelpPanel");
		helpPanel.SetActive (false);
		settingsButtonPanel = GameObject.Find ("SettingsButtonPanel");
		exitButtonPanel = GameObject.Find ("ExitButtonPanel");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenSettings(){

		settingsPanel.SetActive (true);
		mainButtonsPanel.SetActive (false);
	}

	public void settingsOK(){

		settingsPanel.SetActive (false);
		mainButtonsPanel.SetActive (true);
	}

	public void OpenHelp(){

		helpPanel.SetActive (true);
		mainButtonsPanel.SetActive (false);
		settingsButtonPanel.SetActive (false);
		exitButtonPanel.SetActive (false);
	}

	public void exitHelp(){

		helpPanel.SetActive (false);
		mainButtonsPanel.SetActive (true);
		settingsButtonPanel.SetActive (true);
		exitButtonPanel.SetActive (true);
	
	}

	public void Play(){
		StartCoroutine (FadeOut ());
	}

	IEnumerator FadeOut(){
	
		float fadingTime = GameObject.Find("Main Camera").GetComponent<SceneFader>().StartFading(1);
		yield return new WaitForSeconds (2.5f);
		SceneManager.LoadScene ("GameIntro");
	}
}
