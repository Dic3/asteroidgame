﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Script for handling events related to player

public class PlayerMovement : MonoBehaviour {

	// xy-coordinates for possible player positions
	private float leftXPos = -2f;
	private float leftYPos = -2f;
	private float rightXPos = 2f;
	private float rightYPos = -2f;
	private float middleXPos = 0f;
	//private float middleYPos = -1.4f;
	private float middleYPos = -1.3f;

	// Players current position
	private int playerPos;

	// Explosion animations when asteroid is destroyed
	public Transform asteroidExplosion;
	public Transform aeNormal;
	public Transform aeGreen;

	// Explosion animation when rocket is destroyed
	public Transform rocketExplosion;

	// Explosion animatio for bomb
	public Transform bombExplosion;

	//float rotation;
	private Animator anim;

	// Counts how many rocket player has destroyed
	private int destroyedRocketsCount;

	public static bool movable;

	void Start () {

		playerPos = 1;
		destroyedRocketsCount = 0;
		anim = GetComponent<Animator> ();
		anim.enabled = false;
		movable = true;
	} 	

	void Update () {
		
		CheckDestroyedRocketsCount ();
		SelectAsteroidExplosionAnim ();
	
		if(Input.GetKeyDown(KeyCode.LeftArrow) && movable == true)
		{
			if(playerPos > 0){
				playerPos--;
			}
		}

		if(Input.GetKeyDown(KeyCode.RightArrow) && movable == true)
		{
			if (playerPos < 2) {
				playerPos++;
			}
		}
			
		if(playerPos == 0)
		{
			transform.position = new Vector2(leftXPos, leftYPos);
			transform.localEulerAngles = new Vector3 (0, 0, 45);
		}

		if(playerPos == 2)
		{
			transform.position = new Vector2(rightXPos, rightYPos);
			transform.localEulerAngles = new Vector3 (0, 0, 315);
		}

		if(playerPos == 1)
		{
			transform.position = new Vector2(middleXPos, middleYPos);
			transform.localEulerAngles = new Vector3 (0, 0, 0);
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Asteroid") {
			Instantiate(bombExplosion, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), Quaternion.identity);
			Instantiate(asteroidExplosion, new Vector2(coll.gameObject.transform.position.x, coll.gameObject.transform.position.y), Quaternion.identity);
			Destroy (coll.gameObject);
			Score.AddPlusOneToScore ();
			ObjectMovement.IncreaseSpeed();
			CreateObject.createTime = CreateObject.createTime - 0.05f;
			Score.DisplayScore ();
			anim.enabled = true;
			anim.Play ("SpaceMineExplosion", -1, 0f);

		} else if (coll.gameObject.tag == "Rocket") {
			Instantiate(bombExplosion, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), Quaternion.identity);
			Instantiate(rocketExplosion, new Vector2(coll.gameObject.transform.position.x, coll.gameObject.transform.position.y), Quaternion.identity);
			Destroy (coll.gameObject);
			destroyedRocketsCount++;
			LivesLeft.livesLeftRocket--;
			Score.DisplayScore ();
			anim.enabled = true;
			anim.Play ("SpaceMineExplosion",-1, 0f);
			Handheld.Vibrate ();
		}
	}

	// Moves player to the left
	public void MoveLeft(){
	
		if(playerPos > 0 && movable == true){
			playerPos--;
		}
	}

	// Moves player to the right
	public void MoveRight(){

		if (playerPos < 2 && movable == true) {
			playerPos++;
		}
	}

	// Checks how many rockets have been destroyed
	void CheckDestroyedRocketsCount(){

		if (destroyedRocketsCount >= 3) {

			movable = false;
			Time.timeScale = 0;
			Score.CheckIfNewHighScore ();
			Score.SetEndScoreToGameOverPanel ();
			ButtonsAndPanels.gameOverPanel.SetActive (true);
			ButtonsAndPanels.fireButton.GetComponent<Button> ().interactable = false;
			ButtonsAndPanels.pauseButtonPanel.SetActive (false);
			ResetValues.ResetAllValues ();
		}
	}

	// Select which animation to play when asteroid is destroyed
	void SelectAsteroidExplosionAnim(){

		if (Score.score <= 9) {
			asteroidExplosion = aeNormal;
		} else if (Score.score > 9) {
			asteroidExplosion = aeGreen;
		}
	}
}