﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameEndPanel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void RestartGame(){

		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void ExitToMenu(){

		SceneManager.LoadScene ("Main");
	}
}
