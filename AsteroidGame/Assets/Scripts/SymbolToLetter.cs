﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SymbolToLetter : MonoBehaviour {

	public Text letterW;
	public Text letterE;
	public Text letterE2;
	public Text letterA;
	public Text letterR;
	public Text letterC;
	public Text letterO;
	public Text letterM;
	public Text letterI;
	public Text letterN;
	public Text letterG;

	private Color baseColor;
	private Color baseColorFaded;
	private Color unvisibleColor;

	private static float timer;
	public bool aliens;

	public static bool blinkingOn;
	private bool letterSetterOn;
	private bool letterSetterLatterOn;

	// Use this for initialization
	void Start () {

		blinkingOn = false;
		letterSetterOn = true;
		letterSetterLatterOn = false;
		baseColor = new Color32 (176,238,25,255);
		baseColorFaded = new Color32 (176,238,25,70);
		unvisibleColor = new Color32 (255, 255, 255, 0);
		aliens = false;
		letterW.text = "w";
		letterW.color = baseColorFaded;
		letterE.text = "e";
		letterE.color = baseColorFaded;
		letterE2.text = "e";
		letterE2.color = baseColorFaded;
		letterA.text = "a";
		letterA.color = baseColorFaded;
		letterR.text = "r";
		letterR.color = baseColorFaded;
		letterC.text = "c";
		letterC.color = baseColorFaded;
		letterO.text = "o";
		letterO.color = baseColorFaded;
		letterM.text = "m";
		letterM.color = baseColorFaded;
		letterI.text = "i";
		letterI.color = baseColorFaded;
		letterN.text = "n";
		letterN.color = baseColorFaded;
		letterG.text = "g";
		letterG.color = baseColorFaded;
	
	}
	
	// Update is called once per frame
	void Update () {

		SetLetter ();
		SetLetterLatter ();
		ChangeLettersToAliens ();
		SetBlinkingOn ();
	}

	void SetLetter(){

		if (letterSetterOn == true) {
			if (Score.score == 4) {
				letterW.text = "W";
				letterW.color = baseColor;
			} else if (Score.score == 8) {
				letterE.text = "E";
				letterE.color = baseColor;
				letterE2.text = "E";
				letterE2.color = baseColor;
			} else if (Score.score == 3) {
				letterA.text = "A";
				letterA.color = baseColor;
			} else if (Score.score == 9) {
				letterR.text = "R";
				letterR.color = baseColor;
			} else if (Score.score == 5) {
				letterC.text = "C";
				letterC.color = baseColor;
			} else if (Score.score == 6) {
				letterO.text = "O";
				letterO.color = baseColor;
			} else if (Score.score == 7) {
				letterM.text = "M";
				letterM.color = baseColor;
			} else if (Score.score == 2) {
				letterI.text = "I";
				letterI.color = baseColor;
			} else if (Score.score == 1) {
				letterN.text = "N";
				letterN.color = baseColor;
			} else if (Score.score == 10) {
				letterG.text = "G";
				letterG.color = baseColor;
				blinkingOn = true;
				letterSetterOn = false;
				letterSetterLatterOn = true;
			}
		}
	}

	void SetLetterLatter(){
	
		if (letterSetterLatterOn == true) {
			if (Score.score == 11) {
				letterW.color = unvisibleColor;
			} else if (Score.score == 12) {
				letterE.color = unvisibleColor;
			} else if (Score.score == 13){
				letterE2.color = unvisibleColor;
			} else if (Score.score == 14) {
				letterA.color = unvisibleColor;
			} else if (Score.score == 15) {
				letterR.color = unvisibleColor;
			} else if (Score.score == 16) {
				letterC.color = unvisibleColor;
			} else if (Score.score == 17) {
				letterO.color = unvisibleColor;
			} else if (Score.score == 18) {
				letterM.color = unvisibleColor;
			} else if (Score.score == 19) {
				letterI.color = unvisibleColor;
			} else if (Score.score == 20) {
				letterN.color = unvisibleColor;
			} else if (Score.score == 21) {
				letterG.color = unvisibleColor;
				CreateObject.createOn = false;
				ButtonsAndPanels.fireEnabled = true;

			}
		}
	}

	// Changes letters to aliens
	void ChangeLettersToAliens(){
	
		if (aliens == true) {
			letterW.text = "*";
			letterE.text = "*";
			letterE2.text = "*";
			letterA.text = "*";
			letterR.text = "*";
			letterC.text = "*";
			letterO.text = "*";
			letterM.text = "*";
			letterI.text = "*";
			letterN.text = "*";
			letterG.text = "*";

		}
	}

	void SetBlinkingOn(){
	
		if (blinkingOn == true) {
			letterW.GetComponent<BlinkText> ().enabled = true;
			letterE.GetComponent<BlinkText> ().enabled = true;
			letterE2.GetComponent<BlinkText> ().enabled = true;
			letterA.GetComponent<BlinkText> ().enabled = true;
			letterR.GetComponent<BlinkText> ().enabled = true;
			letterC.GetComponent<BlinkText> ().enabled = true;
			letterO.GetComponent<BlinkText> ().enabled = true;
			letterM.GetComponent<BlinkText> ().enabled = true;
			letterI.GetComponent<BlinkText> ().enabled = true;
			letterN.GetComponent<BlinkText> ().enabled = true;
			letterG.GetComponent<BlinkText> ().enabled = true;
		}

	}
}

	
