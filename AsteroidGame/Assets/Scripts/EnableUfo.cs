﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// This script sets Ufo (and ufoLight) object active once player has reached needed score

public class EnableUfo : MonoBehaviour {

	public GameObject ufo;
	public GameObject ufoLight;
	private float timer;
	private bool timerOn;

	void Start () {
	
		timer = 0f;
		timerOn = false;
		ufoLight = GameObject.Find ("UfoLight");
		ufo = GameObject.Find ("Ufo");
		ufoLight.SetActive (false);
		ufo.SetActive (false);
	}
		
	void Update () {
	
		StartTimerForEnablingLight ();

		if (Score.score == 10) {
			ufo.SetActive (true);
			timerOn = true;
		}

		if (timer >= 9.0f){
			ufoLight.SetActive (true);
			ufoLight.GetComponent<UfoLight>().enabled = true;
			timerOn = false;
		}
	}

	// Timer for enabling ufoLight
	private void StartTimerForEnablingLight(){
		if (timerOn == true) {
			timer += Time.deltaTime;
		}
	}
}
