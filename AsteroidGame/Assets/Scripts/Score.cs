﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Script for handling events related to score

public class Score : MonoBehaviour {

	public static int score;
	private static Text scoreText;
	private static Text finalScore;
	private Text[] texts;
	private static bool newHighScore;

	void Start () {

		score = 0;
		scoreText = GameObject.Find("Score").GetComponent<Text>();
		texts = GameObject.Find("Canvas").GetComponentsInChildren<Text> (true);
		finalScore = texts [6];
		newHighScore = false;
		DisplayScore ();
	}
		
	void Update () {

	}

	// Displays score at the top of the window
	public static void DisplayScore(){
		scoreText.text = "ASTEROIDS DESTROYED: " + score.ToString ();
	}

	// Increases score
	public static void AddPlusOneToScore(){
		score++;
	}

	// Sets High Score
	public static void SetHighScore(){
		PlayerPrefs.SetInt ("HighScore", score);
	}

	// Gets High Score
	public static void GetHighScore(){
		PlayerPrefs.GetInt ("HighScore");
	}

	// Checks if new High Score made
	public static void CheckIfNewHighScore(){
		if(score > PlayerPrefs.GetInt("HighScore")){
			SetHighScore();
			newHighScore = true;
		}
	}

	// Sets score of the game to GameOverPanel
	public static void SetEndScoreToGameOverPanel(){
		if (newHighScore == true) {
			finalScore.text = "SCORE\n" + score.ToString() + "\nNEW HIGH SCORE!";
		} else {
			finalScore.text = "SCORE\n" + score.ToString ();
		}
	}
}
