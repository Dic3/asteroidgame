﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AmmoCollision : MonoBehaviour {

	public GameObject ammo;
	public GameObject errorScreen;

	public GameObject ufo;
	public Transform ufoExplosion;
	public Transform ammoExplosion;

	private Color32 ufoRedOverlay;
	private SpriteRenderer ufoSp;

	// Use this for initialization
	void Start () {

		ufoRedOverlay = new Color32 (255, 0, 0, 255);
		ufoSp = ufo.GetComponent<SpriteRenderer> ();
		//ammo = GameObject.Find ("Ammo");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Ammo") {
			Instantiate(ufoExplosion, new Vector2(ufo.transform.position.x, ufo.transform.position.y), Quaternion.identity);
			Instantiate(ammoExplosion, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), Quaternion.identity);
			ammo.SetActive (false);
			ufoSp.color = ufoRedOverlay;
			ResetValues.ResetAllValues ();
			StartCoroutine (SetErrorScreen ());
			StartCoroutine (EndGame ());
			//errorScreen.SetActive (true);
		}
	}

	public IEnumerator SetErrorScreen(){

		yield return new WaitForSeconds (.6f);
		errorScreen.SetActive (true);

	}
	public IEnumerator EndGame(){

		yield return new WaitForSeconds (2.5f);
		GameObject.Find("Main Camera").GetComponent<SceneFaderGame>().StartFading(1);
		yield return new WaitForSeconds (2.5f);
		SceneManager.LoadScene ("GameIntro");

	}

}
