﻿using UnityEngine;
using System.Collections;

public class StopButtonScript : MonoBehaviour {


	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PauseGame(){
	
		Time.timeScale = 0;
	
	}
	public void ResumeGame(){
	
		Time.timeScale = 1;

	}

}
