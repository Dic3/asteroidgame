﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlinkText : MonoBehaviour {

	private Text text;
	private Color textColor;

	// Use this for initialization
	void Start () {

		text = gameObject.GetComponent<Text> ();
		StartCoroutine (ChangeColors ());

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator ChangeColors(){

		for(int i = 0; i<7 ; i++) {
			textColor = new Color32 (176, 238, 25, 255);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 200);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 150);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 100);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 70);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 70);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 100);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 150);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 200);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
			textColor = new Color32 (176, 238, 25, 255);
			text.color = textColor;
			yield return new WaitForSeconds (.15f);
		}
		text.text = "*";
		ButtonsAndPanels.preFirePanelEnabled = true;
	}
}
