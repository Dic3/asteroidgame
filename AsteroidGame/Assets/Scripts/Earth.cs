﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Script for handlig events related to Earth (circle collision) object

public class Earth : MonoBehaviour {

	// Player's "lives"
	private int lives;

	// Explosion prefabs when object hits the earth
	public Transform redCircle;
	public Transform blueCircle;

	// Timer for Earth's overlay color
	private float overlayTimer;

	// Earth
	private GameObject earth;

	// Earth object's SpriteRenderer component
	private SpriteRenderer sp;

	// OverlayColors for earth object
	private Color32 asteroidHit;
	private Color32 rocketHit;
	private Color32 overlayColor;
	private Color32 normalColor;

	void Start () {

		lives = 3;
		Time.timeScale = 1;
		overlayTimer = 3f;
		earth = GameObject.Find ("Earth (1)");
		asteroidHit = new Color32 (255, 50, 0, 255);
		rocketHit = new Color32 (0, 135, 255, 255);
		normalColor = new Color32 (255, 255, 255, 255);
		sp = earth.GetComponent<SpriteRenderer> ();
	}

	void Update () {
	
		overlayTimer += Time.deltaTime;
		SetOverlayColor ();
		CheckLifeCount ();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Asteroid") {
			Instantiate(redCircle, new Vector2(0.04f, -4.52f), Quaternion.identity);
			Destroy (coll.gameObject);
			lives--;
			LivesLeft.livesLeftEarth--;
			overlayColor = asteroidHit;
			overlayTimer = 0f;
			LifeBar.lives--;
			Handheld.Vibrate ();
		
		} else if (coll.gameObject.tag == "Rocket") {
			Instantiate(blueCircle, new Vector2(0.04f, -4.52f), Quaternion.identity);
			Destroy (coll.gameObject);
			overlayColor = rocketHit;
			overlayTimer = 0f;
		}
	}

	// Checks how many "lives" left
	void CheckLifeCount(){

		if (lives <= 0) {
			Time.timeScale = 0;
			PlayerMovement.movable = false;
			Score.CheckIfNewHighScore ();
			Score.SetEndScoreToGameOverPanel ();
			ButtonsAndPanels.gameOverPanel.SetActive (true);
			ButtonsAndPanels.pauseButtonPanel.SetActive (false);
			ButtonsAndPanels.fireButton.GetComponent<Button> ().interactable = false;
			ResetValues.ResetAllValues ();
		}
	}

	// Sets overlaycolor for earth object
	void SetOverlayColor(){

		if (overlayTimer < 0.333f) {
			sp.color = overlayColor;
		} else if (overlayTimer >= 0.333f){
			sp.color = normalColor;
		}
	}
}