﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Script for handling "life" bars

public class LivesLeft : MonoBehaviour {

	public Text earthOne;
	public Text earthTwo;
	public Text earthThree;
	public Text rocketOne;
	public Text rocketTwo;
	public Text rocketThree;

	private Color earthColor;
	private Color rocketColor;
	private Color fadedColor;

	public static int livesLeftEarth;
	public static int livesLeftRocket;

	void Start () {

		earthColor = new Color32 (0, 0, 255, 255);
		rocketColor = new Color32 (255, 0, 0, 255);
		fadedColor = new Color32 (255, 255, 255, 75);

		earthOne.color = earthColor;
		earthTwo.color = earthColor;
		earthThree.color = earthColor;

		rocketOne.color = rocketColor;
		rocketTwo.color = rocketColor;
		rocketThree.color = rocketColor;

		livesLeftEarth = 3;
		livesLeftRocket = 3;
	}

	void Update () {
	
		CheckLivesLeftEarth ();
		CheckLivesLeftRocket ();
	}

	// Check how many "lives" should be shown in earth bar
	void CheckLivesLeftEarth(){

		if (livesLeftEarth == 2) {
			earthThree.color = fadedColor;
		} else if (livesLeftEarth == 1) {
			earthTwo.color = fadedColor;
		} else if (livesLeftEarth == 0) {
			earthOne.color = fadedColor;
		}
	}

	// Check how many "lives" should be shown in rocket bar
	void CheckLivesLeftRocket(){

		if (livesLeftRocket == 2) {
			rocketThree.color = fadedColor;
		} else if (livesLeftRocket == 1) {
			rocketTwo.color = fadedColor;
		} else if (livesLeftRocket == 0) {
			rocketOne.color = fadedColor;
		}
	}
}
