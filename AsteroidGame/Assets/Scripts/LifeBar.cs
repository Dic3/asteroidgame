﻿using UnityEngine;
using System.Collections;

public class LifeBar : MonoBehaviour {

	Animator anim;
	public static int lives;

	// Use this for initialization
	void Start () {
	
		anim = GetComponent<Animator> ();
		lives = 3;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		
		anim.SetInteger ("Lives", lives);
	}

}
